## eclm
Based on the robbyrussell theme.

![eclm theme](https://raw.githubusercontent.com/oh-my-fish/theme-eclm/master/screenshot.png)

### Note:

`fundle plugin 'fisherman/git_util'` is needed in order for the theme to display git status commands

#### Characteristics

* Displays git information in the command prompt when available.
* Indicates 'master' branch with a distinctive color, encouraging the use of feature-branches (useful when development is done using pull requests)
* If the last command was failed, the indicator would be red, otherwise it's green
asdf
