# note: these git functions are pasted from `fundle plugin 'fisherman/git_util'`

function git_branch_name -d "Get the name of the current Git branch, tag or sha1"
    set -l branch_name (command git symbolic-ref --short HEAD ^/dev/null)

    if test -z "$branch_name"
        set -l tag_name (command git describe --tags --exact-match HEAD ^ /dev/null)

        if test -z "$tag_name"
            command git rev-parse --short HEAD ^/dev/null
        else
            printf "%s\n" "$tag_name"
        end
    else
        printf "%s\n" "$branch_name"
    end
end

function pretty_time_since_last_commit
    if git_is_empty
        return 1
    end
    echo (git log --max-count=1 --format='%ar' ^ /dev/null)
end

function git_is_repo -d "Test if the current directory is a Git repository"
    if not command git rev-parse --git-dir >/dev/null ^/dev/null
        return 1
    end
end

function git_is_empty -d "Test if a repository is empty"
    if command git rev-list -n 1 --all >/dev/null ^/dev/null
        return 1
    end
    git_is_repo
end

function repo_has_origin -d "Test if remotes/origin exists"
    set -l originUrl (command git remote get-url origin ^ /dev/null)
    if test -z $originUrl
        return 1
    end
end

function git_sha_abbrev
    echo (git rev-parse --short HEAD ^ /dev/null) | sed -e 's|^refs/heads/||' ^ /dev/null
end

function git_remote_info --argument remoteRef successColor
    if repo_has_origin
        set -l remoteDiff (git rev-list --count --left-right "$remoteRef...HEAD" ^/dev/null)
        if test (echo $remoteDiff | awk '{print $1}') != '0'
            echo "-"
        else if test (echo $remoteDiff | awk '{print $2}') != '0'
            echo "+"
        else
            echo "$successColor✔"
        end
    end
end

function fish_prompt
    set -l last_status $status
    set -l red (set_color red)
    set -l blue (set_color -o '7587a6')
    set -l normal (set_color normal)
    set -l time_color (set_color '5f5a60')
    set -l git_sha_color (set_color '9b703b')

    if test $last_status = 0
        set status_indicator " $blue»$normal"
    else
        set status_indicator " $red✗$normal"
    end
    set -l cwd $time_color(date +%H:%M:%S) ' ' $blue(prompt_pwd)

    if [ (git_branch_name) ]
        set -l git_sha (git_sha_abbrev)
        set -l remoteInfo (git_remote_info 'origin/master')
        set git_info " $blue($git_sha_color$git_sha$blue) $normal$remoteInfo"
    end

    # Notify if a command took more than 5 minutes
    if [ "$CMD_DURATION" -gt 300000 ]
        echo The last command took (math "$CMD_DURATION/1000") seconds.
    end

    echo -n -s $cwd $git_info $status_indicator ' '
end
