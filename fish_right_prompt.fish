function fish_right_prompt
  set -l blue (set_color -o '7587a6')
  set -l commit_time_color (set_color '5f5a60')
  set -l normal (set_color normal)
  set -l green (set_color -o '8f9d6a')
  set -l red (set_color -o 'cf6a48')
  set -l branch_name_color (set_color -o 'cda869')
  set -l orange (set_color -o 'dd7016')
  set -l yellow (set_color -o 'e7e401')

  if [ (git_branch_name) ]
    set git_info $commit_time_color(pretty_time_since_last_commit)

    # these functions defined in fundle/fisherman/git_util
    if git_is_dirty; or git_is_staged
      set git_info "$git_info $red✻"
    end

    if git_is_stashed
      set git_info "$git_info $orange⚑"
    end

    set -l git_branch (git_branch_name)

    set -l remoteInfo (git_remote_info $git_branch $green)
    set git_info "$git_info $yellow$remoteInfo"

    echo -n -s $git_info " $blue($branch_name_color$git_branch$blue)$normal"
  end

end

